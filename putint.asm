[bits 32]
putint: ; prints a u32 from eax in decimal
	pushad
	mov ecx, 0x0a ; divisor for base10
	push word .endnum; end of number we'll print
.divloop:
	xor edx, edx
	div ecx
	push dx
	or eax, eax
	jnz .divloop
.putcloop:
	pop ax
	cmp ax, .endnum
	je end
	add al, 0x30
	call putc
	jmp .putcloop
.end:
	popad
	ret 

.endnum equ 0xe1d
	 
