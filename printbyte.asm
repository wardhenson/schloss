printbyte:
	pusha
	mov ah, al
	shr al, 4
	call .num_or_letr
	call putc
	mov al, ah
	and al, 0x0f
	call .num_or_letr
	call putc
	popa
	ret

.num_or_letr:
	cmp al, 0xA
	jb .is_num
	add al, 0x37;digit is represented with letter 
	ret
.is_num:
	add al, 0x30
	ret
	
