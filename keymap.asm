;the table of characters corresponding to various scancodes
[bits 32]
keymap:
	db 0
	db 0x1B
	db "1234567890-="
	db 0x08
	db 0x09
	db "qwertyuiop[]"
	db 13 
	db 0 ; left ctrl
	db "asdfghjkl;'`"
	db 0 ; left shift
	db "\"
	db "zxcvbnm,./"
	db 0 ; right shift
	db "*"
	db 0 ; left alt
	db " "
	db 0 ; caps lock
times 256-($-keymap) db 0
