[ORG 0x7c00]
KERNEL equ 0x8000
mov [bootdrive], dl
xor ax, ax
mov es, ax
mov ds, ax
mov ss, ax
mov sp, 0x7000
mov bp, 0x7000
mov si, message
call puts
call detectlowmem
call detectram
mov bx, KERNEL 
call rddsk
call switchmode

bravenewworld:
[bits 32]
jmp KERNEL
[bits 16]
jmp $

message:
	db "System Booting", 13, 10, 0
bootdrive:
	db 0

%include "putc.asm"
%include "puts.asm"
%include "rddsk.asm"
%include "printbyte.asm"
%include "switchmode.asm"
%include "detectlowmem.asm"
%include "detectram.asm"
[bits 16]

times 510-($-$$) db 0
db 0x55
db 0xaa

