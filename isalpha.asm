[bits 32]
isalpha: ; checks if the char in al is alphabetic
	cmp al, 'A'
	jl .false
	cmp al, 'z'
	jg .false
	cmp al, 'Z'
	jle .true
	cmp al, 'a'
	jge .true

.false:
	mov al, 0
.true:
	ret	
