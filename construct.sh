[ ! -d bin ] && mkdir bin
nasm boot.asm -o bin/boot
nasm k.asm  -o bin/k
cat bin/boot > bin/flopey
cat bin/k >> bin/flopey
qemu-system-i386 -fda bin/flopey
