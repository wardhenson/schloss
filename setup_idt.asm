[bits 32]
setup_idt:
	pushad
	mov eax, KBD 
	call newline
	call print_dword_hex
	mov [IDT.kbd_dword_low], ax
	rol eax, 16
	mov [IDT.kbd_dword_high], ax
	lidt [.idt_info]
	popad
	ret

.idt_info:
	dw (8*255)
	dd IDT

