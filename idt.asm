; interrupt descriptor table
[bits 32]
IDT:
	;interrupts 0-31, reserved by the processor, empty for now
	times 32 dq 0x00000E0000080000
	;interrupt 0x20, supposed to be timer, empty for now
	dq 0x000000E000080000

	;interrupt 0x21, supposed to be keyboard input
.kbd_dword_low:
	dw 0x0000 ; uninitialized at this point in code
	dw 0x0008 ; first segment

	dw 0x8E00 ; some important info encoded in magic number
.kbd_dword_high:
	dw 0x0000 ; uninitialized at this point in code

	; other interrupts
	times (256-34) dq 0x000000E000080000

KBD equ int_keyboard
