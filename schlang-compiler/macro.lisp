(defun macrolambda (args body)
  (eval `(lambda ,args ,body)))

(defparameter *env-global* '())

(defun makroexpand-program (program-list)                                  
  (let ((env (gather-macro-defs program-list *env-global*)))
    (expand-until-stops-changing program-list env)))

(defun expand-until-stops-changing (old env)
  (if (equal old (find-and-expand-macros old env))
      old
      (expand-until-stops-changing (find-and-expand-macros old env) env)))

(defun macroexpand-single-macro (macroexpr env)
  (let ((macroname (car macroexpr)))
    (apply (get-macrofun-from-env macroname env) (cdr macroexpr))))

(defun find-and-expand-macros (lst env)
  (if (listp lst)
      (if (macro-application-p lst env)
          (macroexpand-single-macro lst env)
          (mapcar (lambda (x) (find-and-expand-macros x env)) lst))
      lst))

(defun macro-application-p (lst env)
  (if (listp lst)
      (get-macrofun-from-env (car lst) env)))

(defun get-macrofun-from-env (name env)
  (cdr (assoc name env)))

(defun gather-macro-defs (lst env)
   (if (and (listp lst) lst)
       (if (macro-def-p lst)
           (add-macro (cadr lst) (macrolambda (caddr lst) (cadddr lst)) env)
           (reduce (lambda (env lst) (gather-macro-defs lst env)) lst :initial-value env))
       env))

(defun add-macro (name fn env)
  (cons (cons name fn) env))

(defun macro-def-p (lst)
  (eq (car lst) 'defmakro))
