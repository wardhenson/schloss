#!/bin/sh
echo "($(cat $1))" | clisp macroexpand-input.lisp | clisp strip-makrodefs.lisp |
	tr A-Z a-z | racket compile-input.rkt > out.asm

