;the table of characters corresponding to various scancodes with shift key
[bits 32]
shift_keymap:
	db 0
	db 0x1B
	db "!@#$%^&*()_+"
	db 0x08
	db 0x09
	db "QWERTYUIOP{}"
	db 13 
	db 0 ; left ctrl
	db 'ASDFGHJKL:"~'
	db 0 ; left shift
	db "|"
	db "ZXCVBNM<>?"
	db 0 ; right shift
	db "*"
	db 0 ; left alt
	db " "
	db 0 ; caps lock
times 256-($-shift_keymap) db 0
