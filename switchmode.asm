switchmode:
	cli
	lgdt [gdt_desc]
	mov eax, cr0
	or eax, 0x01
	mov cr0, eax
	jmp CODE:initsegments

[bits 32]
initsegments:
	mov ax, DATA
	mov ds, ax
	mov ss, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ebp, 0x90000
	mov esp, ebp
;	mov [0xb8000], dword 0x07690748
	jmp bravenewworld	




gdt:	;global descriptor table
	times 8 db 0 ;null descriptor
codeseg:
	dd 0x0000ffff ;code segment descriptor
	dd 00000000_1_1_0_0_1111_1_00_1_1_0_1_0_00000000b
dataseg:
	dd 0x0000ffff ; data segment descriptor
	dd 00000000_1_1_0_0_1111_1_00_1_0_0_1_0_00000000b
gdt_end:

gdt_desc:
	dw gdt_end - gdt - 1 ; gdt size
	dd gdt

CODE equ codeseg - gdt
DATA equ dataseg - gdt
