[bits 32]
print_dword_hex:
	pushad
	mov ecx, 4
.loop:
	rol eax, 8
	call printbyte
	loop .loop
	popad
	ret	

