[bits 32]
mputcxy: ; al - symbol, ebx - video memory, ch - y, cl - x
	pushad

	push eax
	push ecx

	mov al, byte (.width*2)
	shr cx, 8
	mul cl
	and eax, 0x0000ffff
	add ebx, eax

	pop ecx
	pop eax

	and ecx, 0xff
	add ebx, ecx
	add ebx, ecx
	mov ah, 0x02
	mov [ebx], ax
	popad
	ret
	

.width equ 80
.height equ 25
