[bits 32]
setup_interrupts:
	pushad
	call setup_pic
	call setup_idt
	sti
	popad
	ret
	
