[bits 16]
detectlowmem:
	pusha
	clc
	int 0x12
	jc detectlowmem_error
	push ax
	shr ax, 8
	call printbyte
	pop ax
	call printbyte
	mov si, kb_av
	call puts
	mov al, 0x01
	call putc
	popa
	ret
	


detectlowmem_error:
	mov si, detectlowmem_errmsg
	call puts 
	jmp $

detectlowmem_errmsg:
	db "Can't detect low memory", 10, 13, 0
kb_av: 
	db "KB of low mem available", 13, 10, 0

