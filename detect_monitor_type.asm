[bits 32]
detect_monitor_type: ; puts video memory location in eax
.code_none equ 0x00
.code_color equ 0x20
.code_monochrome equ 0x30
	mov ax, [0x410] ; monitor type is located here
	and ax, 0x30
	cmp ax, .code_color
	je .color
	jg .mono
.none:
	mov eax, 0
	jmp .end
.mono:
	mov eax, 0xb0000
	jmp .end
.color:
	mov eax, 0xb8000
.end:
	ret
 
