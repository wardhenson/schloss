[bits 16]
detectram:
	pusha
	mov di, 0xA002
	xor ebx, ebx
	push word 0
.loop:
	mov edx, 0x534D4150 ; another magic number
	xor eax, eax
	mov eax, 0xe820;ram detection magicnum
	mov ecx, 24
	int 0x15
	jc .error
	cmp eax, 0x534D4150 ; success magic number
	jne .error
	cmp ebx, 0
	je .end
	add di, 20
	pop dx
	inc dx
	push dx
	jmp short .loop
.end:
	pop dx
	mov [0xA000], dx
	popa
	ret



.error:
	mov si, .msg
	call puts	
	jmp $

.msg: db "Error detecting ram", 10, 13, 0
