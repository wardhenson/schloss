SECTORS equ 0x08
rddsk:
	pusha
	mov ah, 0x02 ; disk read interrupt
	mov al, SECTORS ; read 1 sector
	mov ch, 0    ; cylinder 0
	mov cl, 2    ; sector 2
	mov dh, 0    ; head 0
	mov dl, [bootdrive]    ; drive 0
	int 0x13
	jc read_error
	cmp al, SECTORS
	jne sectors_error
	popa
	ret
read_error:
	mov si, read_error_msg
	jmp out
sectors_error:
	mov al, ah
	call printbyte
	mov si, sectors_error_msg
out:
	call puts
	jmp $ 



read_error_msg: db "Disk Read Eror", 10, 13, 0
sectors_error_msg: db "Sectors Error", 10, 13, 0

