[bits 32]
scancode_to_char: ;transforms a scancode from al into a printable char
	cmp al, LEFT_SHIFT
	je .on_shift_pressed
	cmp al, RIGHT_SHIFT
	je .on_shift_pressed
	cmp al, (LEFT_SHIFT | 0x80)
	je .on_shift_unpressed
	cmp al, (RIGHT_SHIFT | 0x80)
	je .on_shift_unpressed
	cmp al, (CAPS_LOCK)
	je .on_caps_pressed
	push ebx
	xor ebx, ebx
	mov bl, al
	add ebx, [.keymap]
	mov al, [ebx]
	cmp byte [.caps_on], 0
	je .over
	call .capsify
.over:
	pop ebx
	jmp .end
.on_caps_pressed:
	not byte [.caps_on]
	jmp .end_shift_stuff
.on_shift_pressed:
	mov dword [.keymap], shift_keymap
	mov [.which_shift_was_pressed], al
	jmp .end_shift_stuff
.on_shift_unpressed:
	xor al, 0x80
	cmp al, [.which_shift_was_pressed]
	jne .end_shift_stuff
	mov dword [.keymap], keymap
.end_shift_stuff:
	xor al, al
.end:
	ret
	
.which_shift_was_pressed:
	db 0

.caps_on:
	db 0

.keymap:
	dd (keymap)

LEFT_SHIFT equ  0x2A
RIGHT_SHIFT equ 0x36
CAPS_LOCK equ 0x3A

.capsify:
	push ax
	call isalpha
	cmp al, 0
	je .dont_capsify
	pop ax
	call switchcase
	ret
.dont_capsify:
	pop ax
	ret

