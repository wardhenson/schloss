; keyboard interrupt handler
int_keyboard:
	pushad
	mov al, 0x20
	out PIC1_COMMAND, al ; acknowledge the interrupt
	
	xor eax, eax
	in al, 0x60
	;call printbyte
	call scancode_to_char
	call putc
	
.end:
	popad
	iret

