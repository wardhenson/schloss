[bits 32]
memcpy:
	pushad
	mov edi, eax
	mov esi, ebx
	cld
.loop:
	lodsb
	stosb
	dec ecx
	or ecx, ecx
	jnz .loop
.end:
	popad
	ret
