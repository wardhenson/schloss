[bits 32]
move: ; moves cursor to (cl, ch)
	pushad
.offset:
	mov al, ch
	mov ah, putc.max_column
	mul ah
	and cx, 0x00ff
	add ax, cx
	mov bx, ax
.move:
	mov dx, 0x3d4
	mov al, 0xf
	out dx, al

	mov al, bl
	inc dx
	out dx, al

	dec dx
	mov al, 0xe
	out dx, al

	inc dx
	mov al, bh
	out dx, al
.end:
	popad
	ret

