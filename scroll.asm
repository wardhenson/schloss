[bits 32]
scroll: ;scrolls characters at ebx 1 character up
	pushad
	mov eax, ebx
	add ebx, putc.max_column*2
	mov ecx, (putc.max_row - 1)*putc.max_column*2
	call memcpy

.clear_last_line:	
	mov ebx, eax
	mov al, ' '
	mov ch, putc.max_row - 1
	mov cl, 0
.loop:
	call mputcxy
	inc cl
	cmp cl, putc.max_column
	jl .loop
	
	popad
	ret

