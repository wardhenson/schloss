[bits 32]
getscancode: ;puts the last scancode into eax
	xor eax, eax
	in al, 0x60 ; keyboard input port
	ret
