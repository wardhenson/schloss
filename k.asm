[ORG 0x8000]
[bits 32]
kernel:
	mov [0xb8000], dword 0x02690248
	mov [0xb8004], dword 0x026b024A
	call detect_monitor_type
	mov ebx, eax
	mov si, msg
	call puts
	mov al, 0x01
	mov ecx, 0x0101
	call mputcxy
	mov ecx, 0x0102
	call mputcxy
	mov ecx, 0x0103
	call mputcxy
	mov ecx, 0x0a28
	call mputcxy
	mov si, poem
	call puts
	call puts
	mov eax, 1338
	call putint
	call newline
	
	push dword schputs
	call schlang_entry
	call eax
	call putint	
	call setup_interrupts
	call newline
	mov si, int_msg
	call puts
	mov eax, 0xdeadbeef
	call print_dword_hex
	call newline
	mov ax, [IDT.kbd_dword_high]
	shl eax, 16
	mov ax, [IDT.kbd_dword_low]
	call print_dword_hex
	call newline
	mov eax, KBD
	call print_dword_hex
	call newline
	int 0x21
	jmp $



msg:
	db "Goood morniiing gentlemen!!! I'd like to introduce you all to\
my new Operating System, Weird Bullshit OS (WBOS)!", 13, "Isn't it great?", 13, 0
poem:
	db "Once upon a midnight dreary", 13, \
	   "While I pondered weak and weary", 13, \
	   "Over many acquaint and curious", 13, \
	   "Tomes of forgotten lore", 13, \
	   "While I nodded nearly napping,", 13, \
	   "Suddenly there came a tapping", 13, \
	   "As of someone gently rapping", 13, \
	   "Rapping at my chamber door", 13, \
	   "'Tis some visitor, I muttered", 13, \
	   "Tapping at my chamber door", 13, \
	   "Only this and nothing more", 13, 0
int_msg:
	db "Interrupts set up successfully", 13, 0

%include "mem_puts.asm"
%include "mputc.asm"
%include "mputcxy.asm"
%include "detect_monitor_type.asm"
%include "putint.asm"
%include "scroll.asm"
%include "memcpy.asm"
%include "memset.asm"
%include "move.asm"
%include "newline.asm"
%include "print_dword_hex.asm"
%include "printbyte.asm"

%include "int_keyboard.asm"
%include "idt.asm"
%include "setup_pic.asm"
%include "setup_idt.asm"
%include "setup_interrupts.asm"

%include "keymap.asm"
%include "shift_keymap.asm"
%include "scancode_to_char.asm"
%include "switchcase.asm"
%include "isalpha.asm"

%include "schlib/add1.asm"
%include "schlib/add.asm"
%include "schlib/sub.asm"
%include "schlib/puts.asm"
%include "schlang-compiler/schlang_entry.asm"

empty:
times (17*512)-($-$$) db 0
