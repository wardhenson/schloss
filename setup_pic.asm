; setup programmable interface controller
setup_pic:
	pushad
	mov al, 0x11
	out PIC1_COMMAND, al ; start init
	out PIC2_COMMAND, al
	mov al, 0x20
	out PIC1_DATA, al ; where interrupts start
	mov al, 0x28
	out PIC2_DATA, al
	mov al, 0100b
	out PIC1_DATA, al ; PIC2 is at IRQ2
	mov al, 2
	out PIC2_DATA, al ; PIC2 is subservient to PIC1
	mov al, 0x01
	out PIC1_DATA, al ; PIC1 is in 8086 mode
	out PIC2_DATA, al ; PIC2 is in 8086 mode

	mov al, 11111101b 
	out PIC1_DATA, al ;mask all interrupts except keyboard
	mov al, 11111111b 
	out PIC2_DATA, al ;mask all interrupts

	popad
	ret
	

;some constants
PIC1_COMMAND equ 0x20
PIC1_DATA    equ 0x21
PIC2_COMMAND equ 0xA0
PIC2_DATA    equ 0xA1
