[bits 32]
putc:
	pushad
	cmp al, 13 ; newline character
	je .newline
	cmp al, 0 ; null char
	je .end

.anychar:
	mov cl, [.x]
	mov ch, [.y]
	call mputcxy
	inc byte [.x]	
	cmp [.x], byte .max_column
	jl .end

.newline:
	inc byte [.y]
	mov [.x], byte 0
	cmp [.y], byte .max_row
	jl .end

.scroll:
	dec byte [.y]
	call scroll
		
.end:
	mov cl, [.x]
	mov ch, [.y]
	call move
	popad
	ret

.x: db 0
.y: db 0
.max_column equ 80
.max_row equ 25
